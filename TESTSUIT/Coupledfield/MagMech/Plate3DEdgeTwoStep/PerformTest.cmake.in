# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: syntax cmake; space-indent on; indent-width 2; encoding utf-8;
# kate: auto-brackets on; mixedindent off; indent-mode cstyle;

#-------------------------------------------------------------------------------
# Some important variables.
#-------------------------------------------------------------------------------
SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
set(TESTSUITE_CFSTOOL_MODE "@TESTSUITE_CFSTOOL_MODE@")
set(TESTSUITE_THREADS "@TESTSUITE_THREADS@")
SET(CFS_BINARY_DIR "@CFS_BINARY_DIR@")

#-------------------------------------------------------------------------------
# Paths to some binaries.
#-------------------------------------------------------------------------------
SET(H5DUMP_BINARY "${CFS_BINARY_DIR}/bin/h5dump${CMAKE_EXECUTABLE_SUFFIX}")
SET(CFS_BINARY "${CFS_BINARY_DIR}/bin/cfs")
SET(CFSTOOL_BINARY "${CFS_BINARY_DIR}/bin/cfstool")

IF(WIN32)
  SET(CFS_BINARY "${CFS_BINARY}.bat")
  SET(CFSTOOL_BINARY "${CFSTOOL_BINARY}.bat")
ENDIF()

#-------------------------------------------------------------------------------
# Path to testsuite source directory.
#-------------------------------------------------------------------------------
SET(TESTSUITE_SRC_DIR "@TESTSUITE_DIR@")

#-------------------------------------------------------------------------------
# Path to testsuite output directory.
#-------------------------------------------------------------------------------
SET(TESTSUITE_BIN_DIR "${CFS_BINARY_DIR}/testsuite")

#-------------------------------------------------------------------------------
# Include some macros. Especially the one for generating test names out of
# directory names.
#-------------------------------------------------------------------------------
INCLUDE("${TESTSUITE_SRC_DIR}/TestMacros.cmake")

#-------------------------------------------------------------------------------
# Macro with standard CFS++ test routine.
#-------------------------------------------------------------------------------
MACRO(CFS_TWO_STEP_TEST)
#  SET(CURRENT_TEST_DIR "${TESTSUITE_SRC_DIR}/TESTSUIT/Singlefield/Flow/Stokes3DPerturbed")

  # Generate test names
  GENERATE_TEST_NAME_AND_FILE("${CURRENT_TEST_DIR}")

  # Determine which files need to be copied to TESTSUITE_BIN_DIR
  DETERMINE_TEST_FILE_LIST()

  # Delete previously generated files.
  CLEANUP_TEST_DIR()

  # Copy files over to TESTSUITE_BIN_DIR in order to avoid running concurrent
  # tests in TESTSUITE_SRC_DIR, which might mess up test results. Another reason
  # is, that the source tree does not get cluttered with the output of simulations
  # any more. This makes the use of SVN or Git much more convenient.
  COPY_TEST_FILES("${TEST_FILE_LIST}" "${SKIP_H5REF}")

  # Use default mode if not specified
  if("${CFSTOOL_MODE}" STREQUAL "")
    message("Default CFSTOOL_MODE is set.")
    set(CFSTOOL_MODE ${TESTSUITE_CFSTOOL_MODE})
  else()
    message("Local CFSTOOL_MODE is set.")
  endif()  
  message("Used CFSTOOL_MODE: ${CFSTOOL_MODE}")

  # =====================================
  #  Step I: Perform magnetic simulation  
  # =====================================
  # Owerwrite base name in order to generate 1st pure magnetic simulation
  SET(TEST_FILE_BASENAME "Plate3DEdge-step1")
  
  # Adjust mesh file name
  SET(CFS_ARGS "-mPlate3DEdgeTwoStep.mesh")
  
  # Determine name of .h5ref to be compared
  FILE(GLOB H5REF_FILE "${CURRENT_TEST_DIR}/Plate3DEdge-step1.h5ref")
  
  # Run CFS++ on the simulation
  RUN_TEST_SIMULATION()

  # Run cfstool to diff against the .h5ref.
  DIFF_TEST_RESULTS_CFSTOOL("${EPSILON}" "${CFSTOOL_MODE}")
  
  # =====================================================================
  #  Step II: Perform mechanic simulation with previous results as input
  # =====================================================================
  
  # Determine name of .h5ref to be compared
  FILE(GLOB H5REF_FILE "${CURRENT_TEST_DIR}/Plate3DEdgeTwoStep.h5ref")
  
  # Generate test names
  GENERATE_TEST_NAME_AND_FILE("${CURRENT_TEST_DIR}")
  
  # Run CFS++ on the simulation
  RUN_TEST_SIMULATION()

  # Run cfstool to diff against the .h5ref.
  DIFF_TEST_RESULTS_CFSTOOL("${EPSILON}" "${CFSTOOL_MODE}")
#  MESSAGE("TEST_NAME ${TEST_NAME}")
#  MESSAGE("TEST_FILE_BASENAME ${TEST_FILE_BASENAME}")
#  MESSAGE("CURRENT_TEST_DIR ${CURRENT_TEST_DIR}")
#  MESSAGE("CURRENT_TEST_SUBDIR ${CURRENT_TEST_SUBDIR}")
#  MESSAGE("TEST_FILE_LIST ${TEST_FILE_LIST}")
#  MESSAGE("H5REF_FILE ${H5REF_FILE}")
ENDMACRO(CFS_TWO_STEP_TEST)

CFS_TWO_STEP_TEST()
