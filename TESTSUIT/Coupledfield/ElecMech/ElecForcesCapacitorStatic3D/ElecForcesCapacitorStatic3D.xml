<?xml version="1.0"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
    xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>3D Static Analysis of a Plate Capacitor</title>
        <authors>
            <author>Georg Jank</author>
        </authors>
        <date>2020-11-02</date>
        <keywords>
            <keyword>electrostatic</keyword>
        </keywords>
        <references></references>
        <isVerified>yes</isVerified>
        <description>
            This simulation consists of two conducting plates. Between
            the plates there is an air gap. The lower plate is fixed 
            and the upper plate is attached to a spring (i.e. material
            with poisson number 0). A voltage is applied across the
            plates and the electrostatic forces moves the upper plate 
            towards the lower plate. The aim of this Testcases is to
            test electrostatic forces in a 3D model.
        </description>
    </documentation>
    
    <fileFormats>
        <input>
            <cdb fileName="ElecForcesCapacitorStatic3D.cdb"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml"/>
    </fileFormats>
    
    <domain geometryType="3d">
        <regionList>
            <region name="V_air" material="air"></region>
            <region name="V_upper_electrode" material="solid"></region>
            <region name="V_lower_electrode" material="solid"></region>
            <region name="V_spring" material="spring"></region>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_upper_electrode"/>
            <surfRegion name="S_lower_electrode"/>
        </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Lagrange id="Lagrange1">
            <isoOrder>1</isoOrder>
        </Lagrange>
        <Lagrange id="Lagrange2">
            <isoOrder>2</isoOrder>
        </Lagrange>
    </fePolynomialList>
    
    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <electrostatic>
                <regionList>
                    <region name="V_air" polyId="Lagrange2"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="S_upper_electrode" value="1"></potential>
                    <ground name="S_lower_electrode"></ground>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <surfElemResult type="elecForceDensity">
                        <surfRegionList>
                            <surfRegion name="S_upper_electrode"/>
                        </surfRegionList>
                    </surfElemResult>
                    <surfElemResult type="elecChargeDensity">
                        <surfRegionList>
                            <surfRegion name="S_upper_electrode"/>
                        </surfRegionList>
                    </surfElemResult>    
                    <elemResult type="elecEnergyDensity">
                        <allRegions/>
                    </elemResult>
                    <surfRegionResult type="elecForce">
                        <surfRegionList>
                            <surfRegion name="S_upper_electrode" outputIds="txt"/>
                        </surfRegionList>
                    </surfRegionResult>
                </storeResults>
            </electrostatic>
        </pdeList>
    </sequenceStep>
    
    <sequenceStep index="2">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <mechanic subType="3d">
                <regionList>
                    <region name="V_upper_electrode" polyId="Lagrange2"/>
                    <region name="V_spring" polyId="Lagrange2"/>
                </regionList>
                <bcsAndLoads>
                    <traction name="S_upper_electrode"> 
                        <sequenceStep index="1">
                            <quantity name="elecForceDensity" pdeName="electrostatic"/>
                            <timeFreqMapping>
                                <constant step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </traction>
                    <fix name="S_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <fix name="S_guide">
                        <comp dof="x"/>
                        <comp dof="z"/>
                    </fix>
                </bcsAndLoads> 
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <!--<nodeList>
                            <nodes name="sensor"/>
                        </nodeList>-->
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>
    
</cfsSimulation>
