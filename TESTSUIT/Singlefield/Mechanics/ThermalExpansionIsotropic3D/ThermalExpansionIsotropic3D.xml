<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Thermal Expansion Isotropic 3D</title>
    <authors>
      <author>Florian Toth</author>
    </authors>
    <date>2018-11-10</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>temperature</keyword>
    </keywords>
    <references>Hook's Law, e.g. https://en.wikipedia.org/wiki/Hooke%27s_law</references>
    <isVerified>yes</isVerified>
    <description>
      We assume a linear-elasitic isotrpoic material.
      The geometry is a unit-cube in 3D.
      
      Whe have istroic thermal expansion and unit dT.
      
      The boundary conditions differ in the 3 sequence steps:
      1) all displacements free -> remains stress-free -> strain = applied thermal strain
      
      2) all (boundary) displacements constrained -> zeros strain -> stress = "thermal" stress i.e. C*alpha*dT
      
      3) x-constraint, z,y-free -> uni-axial stress
      
      4) y,z-constraint, x-free -> uni-axial strain
      
      see the IPhython notebook for the analytical solution.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <!--<gmsh fileName="UnitCube_lin.msh"/>-->
      <hdf5 fileName="ThermalExpansionIsotropic3D.h5ref"/>
    </input>
    <output>
      <hdf5 id="hdf" gridId="default"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d" printGridInfo="no">
    <regionList>
      <region name="SOLID" material="ortho"/>
    </regionList>
<!--    <elemList>
      <elems name="center">
        <coord x="0" y="0" z="0"/>
      </elems>
    </elemList>-->
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static> </static>
    </analysis>
    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="SOLID"/>
        </regionList>
        <bcsAndLoads>
          <fix name="SWB">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/>
          </fix>
          <fix name="SOB">
            <comp dof="y"/>
            <comp dof="z"/>
          </fix>
          <fix name="NWB">
            <comp dof="z"/>
          </fix>
          <thermalStrain name="SOLID" value="1.0"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="NOT" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="mechStress">
            <!--<allRegions/>--><!-- do not write to h5, because almost zero values are not robust in testing -->
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="2">
    <analysis>
      <static> </static>
    </analysis>
    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="SOLID"/>
        </regionList>
        <bcsAndLoads>
          <fix name="W">
            <comp dof="x"/>
          </fix>
          <fix name="O">
            <comp dof="x"/>
          </fix>
          <fix name="S">
            <comp dof="y"/>
          </fix>
          <fix name="N">
            <comp dof="y"/>
          </fix>
          <fix name="B">
            <comp dof="z"/>
          </fix>
          <fix name="T">
            <comp dof="z"/>
          </fix>
          <thermalStrain name="SOLID" value="1.0"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <!--<allRegions/>--><!-- do not write to h5, because almost zero values are not robust in testing -->
            <nodeList>
              <nodes name="NOT" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="mechStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechStrain">
            <!--<allRegions/>--><!-- do not write to h5, because almost zero values are not robust in testing -->
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="3">
    <analysis>
      <static> </static>
    </analysis>
    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="SOLID"/>
        </regionList>
        <bcsAndLoads>
          <fix name="W">
            <comp dof="x"/>
          </fix>
          <fix name="S">
            <comp dof="y"/>
          </fix>
          <fix name="B">
            <comp dof="z"/>
          </fix>
          <fix name="O">
            <comp dof="x"/>
          </fix>
          <thermalStrain name="SOLID" value="1.0"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="NOT" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="mechStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="4">
    <analysis>
      <static/> 
    </analysis>
    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="SOLID"/>
        </regionList>
        <bcsAndLoads>
          <fix name="W">
            <comp dof="x"/>
          </fix>
          <fix name="S">
            <comp dof="y"/>
          </fix>
          <fix name="N">
            <comp dof="y"/>
          </fix>
          <fix name="B">
            <comp dof="z"/>
          </fix>
          <fix name="T">
            <comp dof="z"/>
          </fix>
          <thermalStrain name="SOLID" value="1.0"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="NOT" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="mechStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStrain">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          <elemResult type="mechThermalStress">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>
  
</cfsSimulation>
